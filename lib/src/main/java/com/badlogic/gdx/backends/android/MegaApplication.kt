package com.badlogic.gdx.backends.android

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Audio
import com.badlogic.gdx.LifecycleListener

open class MegaApplication : AndroidApplication() {
    private lateinit var audio2: AndroidAudio2

    override fun initialize(listener: ApplicationListener?) {
        initialize(listener, AndroidApplicationConfiguration())
    }

    override fun initialize(listener: ApplicationListener?, config: AndroidApplicationConfiguration) {
        audio2 = AndroidAudio2(context, config)

        addLifecycleListener(object : LifecycleListener {
            override fun resume() {
                // No need to resume audio here
            }

            override fun pause() {
                audio2.pause()
            }

            override fun dispose() {
                audio2.dispose()
            }
        })

        super.initialize(listener, config)
    }

    override fun getAudio(): Audio {
        return audio2
    }
}