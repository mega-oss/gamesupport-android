package com.badlogic.gdx.backends.android

import android.app.Activity
import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import com.badlogic.gdx.Audio
import com.badlogic.gdx.Files
import com.badlogic.gdx.audio.AudioDevice
import com.badlogic.gdx.audio.AudioRecorder
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.utils.GdxRuntimeException
import java.io.File
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

open class AndroidAudio2(private val context: Context, config: AndroidApplicationConfiguration) : Audio {

    private val audio: AndroidAudio = AndroidAudio(context, config)
    private val soundPool: SoundPool?
    private val manager: AudioManager?

    init {

        if (!config.disableAudio) {
            val audioAttrib = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()
            soundPool =
                SoundPool.Builder().setAudioAttributes(audioAttrib).setMaxStreams(config.maxSimultaneousSounds).build()

            manager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
            if (context is Activity) {
                context.volumeControlStream = AudioManager.STREAM_MUSIC
            }
        } else {
            soundPool = null
            manager = null
        }
    }

    fun pause() {
        if (soundPool == null) {
            return
        }
        audio.pause()
        this.soundPool.autoPause()
    }

    fun resume() {
        if (soundPool == null) {
            return
        }
        audio.resume()
        this.soundPool.autoResume()
    }

    /**
     * {@inheritDoc}
     */
    override fun newAudioDevice(samplingRate: Int, isMono: Boolean): AudioDevice {
        return audio.newAudioDevice(samplingRate, isMono)
    }

    /**
     * Fixes loading of music files from classpath
     */
    override fun newMusic(fh: FileHandle): Music {
        if (fh.type() == Files.FileType.Classpath) {
            val mediaPlayer = MediaPlayer()
            try {
                val bytes = fh.readBytes()
                val hash = sha(bytes)
                val existing = File(context.filesDir, hash)

                if (!existing.exists() || hash != existing.name) {
                    val fo = context.openFileOutput(hash, Context.MODE_PRIVATE)
                    fo.write(bytes)
                    fo.flush()
                    fo.close()
                }

                mediaPlayer.setDataSource(existing.path)
                mediaPlayer.prepare()
                val music = AndroidMusic(audio, mediaPlayer)
                synchronized(audio.musics) {
                    audio.musics.add(music)
                }
                return music
            } catch (ex: Exception) {
                throw GdxRuntimeException("Error loading music file: $fh", ex)
            }

        } else {
            return audio.newMusic(fh)
        }
    }

    /**
     * Fixes loading of sound files from classpath
     */
    override fun newSound(fh: FileHandle): Sound {
        return if (fh.type() == Files.FileType.Classpath) {
            try {
                val existing = File(context.filesDir, fh.name())
                val bytes = fh.readBytes()
                val fo = context.openFileOutput(fh.name(), Context.MODE_PRIVATE)
                fo.write(bytes)
                fo.flush()
                fo.close()

                val soundId = soundPool!!.load(existing.path, 1)
                AndroidSound(soundPool, manager, soundId)

            } catch (ex: Exception) {
                throw GdxRuntimeException("Error loading sound file: $fh", ex)
            }

        } else {
            audio.newSound(fh)
        }
    }

    /**
     * {@inheritDoc}
     */
    override fun newAudioRecorder(samplingRate: Int, isMono: Boolean): AudioRecorder {
        return audio.newAudioRecorder(samplingRate, isMono)
    }

    /**
     * Kills the soundpool and all other resources
     */
    fun dispose() {
        audio.dispose()

        if (soundPool == null) {
            return
        }
        soundPool.release()
    }

    private fun sha(input: ByteArray): String {
        val digest: MessageDigest
        try {
            digest = MessageDigest.getInstance("SHA-256")
            val hash = digest.digest(input)
            return bytesToHex(hash)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return bytesToHex(input)
    }

    private fun bytesToHex(hash: ByteArray): String {
        val hexString = StringBuilder()
        for (aHash in hash) {
            val hex = Integer.toHexString(0xff and aHash.toInt())
            if (hex.length == 1) hexString.append('0')
            hexString.append(hex)
        }
        return hexString.toString()
    }
}