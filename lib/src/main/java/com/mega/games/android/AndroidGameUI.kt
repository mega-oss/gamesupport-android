package com.mega.games.android

import android.app.Dialog
import android.view.KeyEvent
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.badlogic.gdx.backends.android.MegaApplication
import com.mega.games.support.MegaServices
import com.mega.games.support.R
import com.mega.games.support.ui.GameUIManager

open class AndroidGameUI(
    private val host: MegaApplication,
    private val services: MegaServices
) : GameUIManager {

    private var onPlayAgainListener: (() -> Unit)? = null
    private var onQuitListener: (() -> Unit)? = null

    private var score = 0L

    override fun setOnPlayAgainListener(listener: () -> Unit) {
        onPlayAgainListener = listener
    }

    override fun setOnQuitListener(listener: () -> Unit) {
        onQuitListener = listener
    }

    override fun showGameOverScreen(score: Long) {
        services.analytics().logEvent("game_over", mapOf("score" to score))
        this.score = score

        host.runOnUiThread {
            Dialog(host, R.style.DialogTheme).apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)

                val view = host.layoutInflater.inflate(R.layout.sample_gameover, null)
                val scoreTV = view.findViewById<TextView>(R.id.scoreTV)
                scoreTV.text = "Score = $score"
                val quitButton = view.findViewById<Button>(R.id.quit_button)
                val retryButton = view.findViewById<Button>(R.id.retry_button)
                quitButton.setOnClickListener { onQuitListener?.invoke() }
                retryButton.setOnClickListener {
                    cancel()
                    onPlayAgainListener?.invoke()
                }

                setContentView(view)
                setCanceledOnTouchOutside(false)
                setOnKeyListener { _, keyCode, _ ->
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        cancel()
                        onQuitListener?.invoke()
                    }
                    false
                }
                show()
            }
        }
    }

}