package com.mega.games.android

enum class GameState {
    LOADING, RUNNING, PAUSED, FINISHED;

    fun isRunning() = equals(RUNNING)
    fun isPaused() = equals(PAUSED)
}