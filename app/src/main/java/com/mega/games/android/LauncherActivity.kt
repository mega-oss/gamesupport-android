package com.mega.games.android

import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.badlogic.gdx.backends.android.MegaApplication
import com.mega.games.android.samplegame.SampleGame
import com.mega.games.android.samplegame.SampleMegaServices
import com.mega.games.support.MegaGame

class LauncherActivity : MegaApplication() {
    private lateinit var game: MegaGame

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startGame()
    }

    private fun startGame() {
        val services = SampleMegaServices()
        game = SampleGame(services)

        val gameUI = AndroidGameUI(this, services).apply {
            setOnPlayAgainListener {
                game.restartGame()
            }

            setOnQuitListener {
                finish()
            }
        }

        game.setGameUIManager(gameUI)

        initialize(game, AndroidApplicationConfiguration().apply {
            useAccelerometer = false
            useCompass = false
        })
    }

    override fun onBackPressed() {
        if (game.isPlaying()) {
            game.forceGameOver()
        }
    }
}