package com.mega.games.android.samplegame

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.loaders.FileHandleResolver
import com.mega.games.support.MegaServices

class SampleMegaServices : MegaServices() {
    override fun fileResolver(): FileHandleResolver = FileHandleResolver { fileName -> Gdx.files.internal(fileName) }
}