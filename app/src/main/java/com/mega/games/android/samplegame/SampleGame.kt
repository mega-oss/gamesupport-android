package com.mega.games.android.samplegame

import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.mega.games.android.GameState
import com.mega.games.support.MegaGame
import com.mega.games.support.MegaServices

class SampleGame(private val mega: MegaServices) : MegaGame(mega) {

    //all assets must be loaded through either the asset manager OR using fileResolver directly
    val assetManager = AssetManager(mega.fileResolver())

    //Game state
    var state = GameState.LOADING
    var score = 0L
    private lateinit var batch: SpriteBatch

    override fun create() {
        super.create()

        //load all assets
        assetManager.load("ship_blue.png", Texture::class.java)
        assetManager.finishLoading()
        mega.callbacks().loaded()

        batch = SpriteBatch()

        restartGame()
    }

    override fun forceGameOver() {
        if (state.isRunning()) {
            state = GameState.FINISHED
            mega.callbacks().gameOver(score)
        }
    }

    override fun restartGame() {
        score = 0L

        state = GameState.LOADING
        setScreen(SampleGameScreen(this, batch))
        state = GameState.RUNNING

        mega.callbacks().playStarted()
    }

    override fun dispose() {
        super.dispose()
        assetManager.dispose()
        batch.dispose()
    }
}

class SampleGameScreen(private val game: SampleGame, private val batch: SpriteBatch) : ScreenAdapter() {

    private val radius = 120f
    private var angle = 0f
    private var angularVel = 50f  // unit: deg/ms
    private var center = Vector2(0f, 0f)
    private var position = Vector2(0f, 0f)

    //assets
    private lateinit var texture: Texture
    private lateinit var image: Sprite

    override fun show() {
        texture = game.assetManager.get("ship_blue.png")
        image = Sprite(texture).apply {
            setOrigin(0f, 0f)
        }
    }

    override fun resize(width: Int, height: Int) {
        center = Vector2(width / 2f, height / 2f)
    }

    private fun update(delta: Float) {
        if (game.state.isRunning()) {
            angle = (angle + angularVel * delta) % 360
            position.x = center.x + MathUtils.cosDeg(angle) * radius - image.width / 2
            position.y = center.y + MathUtils.sinDeg(angle) * radius - image.height / 2

            game.score = position.x.toLong()
        }
    }

    override fun render(delta: Float) {
        if (game.state != GameState.LOADING) {
            update(delta)

            clearScreen(0.8f, 0.8f, 0.8f)
            batch.use {
                //show game screen
                it.draw(image, position.x, position.y)
            }
        }
    }
}
