Support library for all mega games built with libgdx

[![Release](https://jitpack.io/v/com.gitlab.mega-oss/gamesupport-android.svg)]
(https://jitpack.io/#com.gitlab.mega-oss/gamesupport-android)

## Installation

add the jitpack.io repository to your **root** build.gradle file:

```code
allprojects {
 repositories {
    jcenter()
    maven { url "https://jitpack.io" }
 }
}
```
and this to your game module's build.gradle (usually core/build.gradle):
```code
dependencies {
    compile 'com.gitlab.mega-oss:gamesupport-android:2.4.1'
}
```
Note: do not add the jitpack.io repository under buildscript

